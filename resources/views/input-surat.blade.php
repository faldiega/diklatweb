@extends('layout.app')
@section('title','Input Surat')
@php $page = 'inputsurat'; @endphp
@section('content')
<div class="container">
    <div class="text-center mt-5">
        <h3>INPUT SURAT USULAN CALON PESERTA PELATIHAN JFK</h3>
        <br><br>      
    </div>

    <form action="/input-surat/store" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Surat</label>
            <div class="col-sm-10">
                <input type="text" name="nosurat" id="nosurat" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tanggal Surat</label>
            <div class="col-sm-10">
                <input type="date" name="tglsurat" id="tglsurat" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Perihal</label>
            <div class="col-sm-10">
                <input type="text" name="perihal" id="perihal" class="form-control">
            </div>
        </div>
        <!-- <div class="form-group row">
            <label class="col-sm-2 col-form-label">File Surat</label>
            <div class="col-sm-10">
                <input type="text" name="filesurat" id="filesurat" class="form-control">
            </div>
        </div> -->
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">File Surat</label>
            <div class="col-sm-10">
                <input type="file" name="filesurat" id="filesurat">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <button type="submit" class="btn btn-success">Simpan</button>
                <a href="/" class="btn btn-danger">Batal</a>
            </div>
        </div>
    </form>
    
</div>    
@endsection
