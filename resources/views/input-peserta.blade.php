@extends('layout.app')
@section('title','Input Peserta')
@php $page = 'inputpeserta'; @endphp
@section('content')
<div class="container">
    <div class="text-center mt-5">
        <h3>INPUT DATA CALON PESERTA PELATIHAN JFK</h3>
        <br><br>      
    </div>
    <form action="" method="post">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">NIP</label>
            <div class="col-sm-10">
                <input type="text" name="nip" id="nip" class="form-control" autofocus>
            </div>
        </div>
        <hr>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nama</label>
            <div class="col-sm-10">
                <input type="text" name="nama" id="nama" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">TMT PNS</label>
            <div class="col-sm-10">
                <input type="text" name="tmtpns" id="tmtpns" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">SK CPNS</label>
            <div class="col-sm-10">
                <input type="text" name="skcpns" id="skcpns" class="form-control">
            </div>
        </div><div class="form-group row">
            <label class="col-sm-2 col-form-label">SK PNS</label>
            <div class="col-sm-10">
                <input type="text" name="skpns" id="skpns" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Pangkat</label>
            <div class="col-sm-10">
                <input type="text" name="pangkat" id="pangkat" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">SK KP Terakhir</label>
            <div class="col-sm-10">
                <input type="text" name="skkpterakhir" id="skkpterakhir" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Unit Kerja</label>
            <div class="col-sm-10">
                <input type="text" name="unitkerja" id="unitkerja" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Pendidikan Terakhir</label>
            <div class="col-sm-10">
                <input type="text" name="pendidikanterakhir" id="pendidikanterakhir" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Ijazah</label>
            <div class="col-sm-10">
                <input type="text" name="ijazah" id="ijazah" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Jenis Pelatihan</label>
            <div class="col-sm-10">
                <select name="pelatihan" id="pelatihan" class="custom-select">
                    <option selected disabled="">-- Pilih Jenis Pelatihan --</option>
                    <option value="">Pelatihan 1</option>
                    <option value="">Pelatihan 2</option>
                    <option value="">Pelatihan 3</option>
                </select> 
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Jenis Usulan Formasi</label>
            <div class="col-sm-10">
                <select name="usulanformasi" id="usulanformasi" class="custom-select">
                    <option selected disabled="">-- Pilih Jenis Usulan Formasi --</option>
                    <option value="">Usulan Formasi 1</option>
                    <option value="">Usulan Formasi 2</option>
                    <option value="">Usulan Formasi 3</option>
                </select> 
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="text" name="email" id="email" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nomor Telp/HP</label>
            <div class="col-sm-10">
                <input type="text" name="notelp" id="notelp" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                <a href="/" class="btn btn-danger btn-sm">Batal</a>
            </div>
        </div>
    </form>
</div>    
@endsection