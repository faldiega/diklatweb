@extends('layout.app')
@section('title','Menu Usulan')
@php $page = "home"; @endphp
@section('content')
<div class="container">
    <div class="text-center mt-5">
        <h3>USULAN CALON PESERTA PELATIHAN JFK</h3>
        <br><br>
        <table class="table table-bordered table-striped">
            <thead class="thead-light">
                <tr>
                    <th>No.</th>
                    <th>No. Surat</th>
                    <th>Tanggal Surat</th>
                    <th>Perihal</th>
                    <th>Jumlah Usulan Peserta</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php $no=1 @endphp
                @foreach ($surat as $sur)
                <tr>
                    <td>@php echo $no; @endphp</td>
                    <td>{{ $sur->nosurat }}</td>
                    <td>{{ $sur->tglsurat }}</td>
                    <td>{{ $sur->perihal }}</td>
                    <td></td>
                    <td>
                        <a href="/input-peserta" class="btn btn-success btn-sm">Tambah Peserta</a>
                        <button type="submit" class="btn btn-warning btn-sm">Detail</button>
                    </td>
                </tr>
                @php $no++ @endphp
                @endforeach
            </tbody>
        </table>
    </div>
    <a href="/input-surat" class="btn btn-success">+ Tambah Surat Usulan</a>
</div>    
@endsection
