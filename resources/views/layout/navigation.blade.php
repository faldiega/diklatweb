<nav class="navbar navbar-expand-lg navbar-dark bg-info">
  <div class="container">
    <a class="navbar-brand" href="/">Web Diklat</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav ml-auto">
        <a class="nav-item nav-link <?php if($page == 'home'){echo 'active';} ?>" href="/">Home<span class="sr-only">(current)</span></a>
        <a class="nav-item nav-link <?php if($page == 'inputsurat'){echo 'active';} ?>" href="/input-surat">Input Surat</a>
        <a class="nav-item nav-link <?php if($page == 'inputpeserta'){echo 'active';} ?>" href="/input-peserta">Input Peserta</a>
      </div>
    </div>
  </div>
</nav>