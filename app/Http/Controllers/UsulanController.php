<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Surat;
class UsulanController extends Controller
{
    public function index()
    {
    	$surat = Surat::all();
    	return view('index', ['surat' => $surat]);
    }

    public function tambah()
    {
    	return view('input-surat');
    }

    public function store(Request $req)
    {
    	$this->validate($req, [
    		'nosurat' => 'required',
    		'tglsurat' => 'required',
    		'perihal' => 'required',
            // 'filesurat' => 'required|file|image|mimes:jpeg,png,jpg,pdf|max:2048',
    		'filesurat' => 'required|mimes:jpeg,png,jpg,pdf|max:2048',
    	]);

        // menyimpan data file yang diupload ke variabel $file
        $file = $req->file('filesurat');
 
        $nama_file = time()."_".$file->getClientOriginalName();
 
        // nama folder (file_surat) tempat file diupload, akan muncul pada folder public secara otomatis
        $tujuan_upload = 'file_surat';
        $file->move($tujuan_upload,$nama_file);

    	Surat::create([
    		'nosurat' => $req->nosurat,
    		'tglsurat' => $req->tglsurat,
    		'perihal' => $req->perihal,
    		'filesurat' => $nama_file,
    	]);

    	return redirect('/');
    }


}
