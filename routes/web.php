<?php
// Route untuk surat
Route::get('/', 'UsulanController@index');
Route::get('/input-surat', 'UsulanController@tambah');
Route::post('/input-surat/store', 'UsulanController@store');

// Route untuk peserta
Route::get('/input-peserta', function(){
	return view('input-peserta');
});
